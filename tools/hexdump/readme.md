un hexdump
==========

Le fichier `HexDump.sml` vient de https://bitbucket.org/jherron/llu. Je n'ai fait que rajouter `main.sml`. Pour la compilation avec _PolyML_, il suffit de faire :

```
polyc -o hexdump main.sml
```


(*

file from https://bitbucket.org/jherron/llu

Copyright (C) 2012 Jon Herron

This file is part of LLU.

LLU is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LLU is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LLU.  If not, see <http://www.gnu.org/licenses/>.

*)

structure HexDump :> sig

val dumpHex : string -> unit

end = struct

fun printHeader () =
    let
      val columns = "87654321  00 11 22 33 44 55 66 77 88 99 aa bb cc dd ee ff  0123456789abcdef\n"
      val separator = String.translate (fn #"\n" => "\n" | _ => "-") columns
    in
      print (columns ^ separator)
    end

val lowerCase = String.map Char.toLower

val printAsciiTranslation = 
    List.app (fn NONE => print " "
	       | SOME byte => 
		 let
		     val chr = Byte.byteToChar byte
		 in
		     print (if Char.isPrint chr then String.str chr else ".")
		 end)

val printByte = fn
    SOME byte => 
    let
      val hex = lowerCase (Word8.toString byte)
    in
      print (StringCvt.padLeft #"0" 2 hex)
    end
  | NONE => print "  "

fun printBytes (byte1 :: byte2 :: bytes) = (printByte byte1; print " "; printByte byte2; print " "; printBytes bytes)
  | printBytes _ = print " "

fun printOffset offset = 
    let
      val hex = lowerCase (LargeInt.fmt StringCvt.HEX offset)
      val paddedOffset = StringCvt.padLeft #"0" 8 hex
    in
      print (paddedOffset ^ ": ")
    end

fun dumpHexFromStream stream = 
    let
      val bytesToRead = 16
      
      fun dumpHexFromStream offset =
          let
            val bytes = List.tabulate (bytesToRead, (fn _ => BinIO.input1 stream))
            val bytesRead = List.length (List.filter Option.isSome bytes)
          in
            if bytesRead = 0 then
                ()
            else 
                (printOffset offset; 
                 printBytes bytes;
                 printAsciiTranslation bytes;
                 print "\n";

                 if bytesRead < bytesToRead then 
                   () 
                 else 
                   dumpHexFromStream (offset + (LargeInt.fromInt bytesToRead)))
          end
    in
      dumpHexFromStream 0
    end

fun dumpHex file = 
    let	
      val () = printHeader ()
      val stream = BinIO.openIn file
    in
      dumpHexFromStream stream before BinIO.closeIn stream
    end

end

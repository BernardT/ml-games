(*
 * main.sml
 *)

use "HexDump.sml";
use "../../blib/BtIO.sml";
open HexDump;
open BtIO

fun main() =
let
  fun dohelp() =
    (printLn ["hexdump [-h] : this text"];
     printLn ["hexdump file file ... : show hexdump of each files"];
     OS.Process.exit OS.Process.success;
     {}
    );

  fun loop [] = {}
    | loop (h :: t) = (
            dumpHex h;
            loop t
    );

  val args = CommandLine.arguments ()  
in
  case (args)
    of ("-h" :: t) => dohelp()
     | ("--help" :: t) => dohelp()
     | (h :: t) => loop args
     | [] => dohelp()
end;

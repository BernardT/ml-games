(*
 * Help.fs
 * affichage de l'aide
 *)

module Help

open Constants

(*
 * dohelp exitValue
 * affiche un texte d'aide et retourne son paramètre
 * exitValue :  code de sortie du programme. Si exitValue n'est
 *      pas nul, un message est envoyé sur la console d'erreur
 *      en fonction de la valeur de ExitValue
 * return: le paramètre exitValue
 *)
let dohelp exitValue moreText =

    let buildErrorMessage errorName =
        "ERROR: " + errorName + " -- " + moreText 

    let firstMessage = function
        | Constants.ExitErrorBadArgs -> buildErrorMessage "Bad typed argument"
        | Constants.ExitErrorTooMuchArgs -> buildErrorMessage "Too much arguments"
        | Constants.ExitErrorUnkownArg -> buildErrorMessage "Unknown argument"
        | Constants.ExitErrorNegative -> buildErrorMessage "Negative argument"
        | Constants.ExitCantOpenFile -> buildErrorMessage "Cannot open file"
        | Constants.ExitCantFindFile -> buildErrorMessage "Cannot Find file"
        | _ -> buildErrorMessage ""

    if exitValue <> ExitSuccess then eprintf "%s\n" (firstMessage exitValue)
    printfn "Usage:"
    printfn "   hexdump.exe [--help] : this text"
    printfn "   hexdump.exe file1 file2 ... : dump file1, file2... on stdout"
    exitValue;;


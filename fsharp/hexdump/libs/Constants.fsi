﻿(*
 * Constants.fsi
 *)

module Constants
    [<LiteralAttribute ()>]
    val ExitSuccess : int = 0;;

    [<LiteralAttribute ()>]
    val ExitErrorNegative : int = 1;;

    [<LiteralAttribute ()>]
    val ExitErrorBadArgs : int = 2;;

    [<LiteralAttribute ()>]
    val ExitErrorUnkownArg : int = 3;;

    [<LiteralAttribute ()>]
    val ExitCantOpenFile : int = 4;;

    [<LiteralAttribute ()>]
    val ExitCantFindFile : int = 5;;

    [<LiteralAttribute ()>]
    val ExitErrorTooMuchArgs : int = 9;;

    [<LiteralAttribute ()>]
    val ExitErrorUnknown : int = 254;;


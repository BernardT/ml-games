﻿(*
 * Constants.fs
 *)


module Constants

    [<Literal>]
    let ExitSuccess = 0

    [<Literal>]
    let ExitErrorNegative = 1

    [<Literal>]
    let ExitErrorBadArgs = 2

    [<Literal>]
    let ExitErrorUnkownArg = 3

    [<Literal>]
    let ExitCantOpenFile = 4

    [<Literal>]
    let ExitCantFindFile = 5

    [<Literal>]
    let ExitErrorTooMuchArgs = 9

    [<Literal>]
    let ExitErrorUnknown = 254


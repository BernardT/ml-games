module Hexdump

open System 
open System.IO
open Constants
open Help 

exception ExceptionCantOpenFile of string

let sysExceptionToMessage (ex : System.Exception) =
    ex.Message + " -- " + ex.Source + "\n -- " + ex.TargetSite.ToString() + "\n -- " + ex.StackTrace

let fileReader fileName bufferSize =
    try
        let fStream : FileStream = new FileStream(fileName, FileMode.Open)
        let bReader : BinaryReader = new BinaryReader(fStream)

        bReader.Close()
        fStream.Close()
        ExitSuccess
    with
        | :? System.Exception as ex -> dohelp ExitErrorUnknown ("[fileReader] " + (sysExceptionToMessage ex))

let doHexdump (fileName:string) : int =
    try
        printfn "Hexdump of %s" fileName
        fileReader fileName 16
    with 
        | :? System.Exception as ex -> dohelp ExitErrorUnknown ("[doHexdump] " + (sysExceptionToMessage ex))

let rec onCommanLine argv = 
    match argv with
    | [] -> ExitSuccess
    | "-h" :: _  -> Help.dohelp ExitSuccess ""
    | "--help" :: _  -> Help.dohelp ExitSuccess ""
    | fileName :: tail  -> let result = doHexdump fileName
                           in if result = ExitSuccess then onCommanLine tail else result;;



[<EntryPoint>]
let main argv =
    let onArgs = function
        | 0 -> Help.dohelp ExitSuccess ""
        | _ -> onCommanLine (List.ofArray argv)

    onArgs argv.Length;;

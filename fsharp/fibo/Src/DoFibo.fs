﻿(*
 * DoFibo.fs
 * calcul et affichage des nombres de Fibonacchi
 *)

module DoFibo

open Printer
open Constants
open Help

(*
 * exception déclenchée en cas de nombre négatif
 *)

exception ExceptionNegativeNumber of string

(*
 * memoiseFibo n
 * mémoïsation de la suite de fibonacci. cette fonction est une fermeture qui garde 
 * le tableau des valeurs calculées en mémoire
 * n : le nombre de Fibonacchi à calculer
 * kont : une continuation ayant n et fibo de n comme paramètres
 * return : renvoie la vraie fonction de calcul de Fibonacci
 *)
let memoiseFibo n kont =
    (* calcul de la taille du tableau de mémoïzation *)
    let memoArraySize = if n < 3
                        then 3
                        else n + 1

    (* création du tableau de mémoïzation *)
    let (memo : bigint array) = Array.zeroCreate memoArraySize

    (* fonction de calcul des nombres de fibonacchi *)
    let rec getFibo = function
        | 0 -> 0I
        | 1 -> 1I
        | n -> let m = memo.[n]
               in
                    if m = 0I 
                        then
                            let newM = getFibo (n - 1) + getFibo (n - 2)
                            memo.[n] <- newM
                            newM
                        else m

    let kGetFibo n =
        let result = getFibo n 
        in 
            kont n result 
        
    kGetFibo;;

(*
 * doFibo str_numberOfTests
 * boucle principale
 * str_numberOfTests : le nombre de valeurs à calculer et afficher
 * return le code de sortie du programme
 *)
let doFibo (str_numberOfTests:string) : int =
    try
        (* nombre de valeurs à calculer et afficher transformer en int *)
        let numberOfLoops = int str_numberOfTests
        (* génération d'une exception si ce nombre est négatif *)
        if numberOfLoops <= 0
            then raise (ExceptionNegativeNumber ("Error: Negative number!"))
            else printfn ""

        (* fonction d'affichage d'une valeur *)
        let fiboPrinter = printIntegerFunction "Fibo"
        let showFibo = memoiseFibo numberOfLoops fiboPrinter

        (* lancement des calculs *)
        showAllFValues showFibo numberOfLoops |> ignore
        ExitSuccess

    with
        (* gestion des exceptions *)
        | ExceptionNegativeNumber(str) -> dohelp ExitErrorNegative
        | :? System.FormatException -> dohelp ExitErrorBadArgs
        | _ -> dohelp ExitErrorUnknown;;


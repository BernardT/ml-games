module Printer

let printIntegerFunction message =
    let doPrint n result =
        printfn "%s %d -> %s" message n (string result)
    in
        doPrint

(* fonction d'affichage de toutes les valeurs *)
let rec showAllFValues showOne = function
    | 0 -> showOne 0
    | n -> showOne n |> ignore
           showAllFValues showOne (n - 1) 


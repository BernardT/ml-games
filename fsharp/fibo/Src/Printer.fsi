module Printer

    val printIntegerFunction : string -> (int -> obj -> unit);;
    val showAllFValues : (int -> 'a) -> int -> 'a;;

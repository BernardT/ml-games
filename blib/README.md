blib
====

Le répertoire de mes bibliothèques _SML_. Elles correspondent surtout à un besoin d'expérimentation et ne sont pas forcément de bonne qualité.

BtIO.sml
--------

C'est ensemble de fonctions censées me faciliter la tâche pour la gestion des entrées/sorties. Voici la signature :

```ml
signature BTIO =
    sig
      datatype exitCode = SUCCESS | FAILURE


      val printInt : int -> unit
      val printIntInf : IntInf.int -> unit
      val printReal : real -> unit

      val onError : string -> int

      val printL : string list -> unit
      val printLn : string list -> unit

      val printValLN: string -> int -> unit;
      val printValEQ: string -> int -> unit;
    end;
```

Lambda.sml
----------

Surtout là pour m'entraîner avec les modules, le code est là pour le lambda calcul - et se réduit à la simple fonction identité.

GetArgs.sml
-----------

Une lecture simpliste des arguments de la ligne de commande. En fait, lecture d'un argument entier positif... Rien de plus actuellement!

```ml
signature GETARGS =
sig
  exception BadArgument;
  exception NegativeArgument;
  exception NoArgument;

  val get_arg_pos_int: string list -> int
end;
```

ShowResults.sml
---------------

Permet l'affichage de résultats de calcul de fonctions sur les entiers.

```ml
signature SHOWRESULTS =
sig
  val show_all_int_results: string -> (int -> IntInf.int) -> int -> unit
end;

```
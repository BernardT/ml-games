(*
 * BtIO.sml
 *)

signature BTIO =
    sig
      datatype exitCode = SUCCESS | FAILURE

      
      val printInt : int -> unit
      val printIntInf : IntInf.int -> unit
      val printReal : real -> unit

      val onError : string -> int

      val printL : string list -> unit
      val printLn : string list -> unit

      val printValLN: string -> int -> unit;
      val printValEQ: string -> int -> unit;
    end;

structure BtIO: BTIO =
struct
  datatype exitCode = SUCCESS | FAILURE

  fun printL [] = {}
    | printL (h :: t) = (
      print h;
      printL t
    );

  fun printLn listOfString = (
    printL listOfString;
    print ("\n")
    );

  fun printInt x = print (Int.toString x)
  fun printIntInf x = print (IntInf.toString x)
  fun printReal x = print (Real.toString x)

  fun onError message = (
    printLn ["ERROR! ", message];
    OS.Process.exit OS.Process.failure;
    1
    );

  fun printValLN message intValue = 
    printLn [message, (Int.toString intValue)];

  fun printValEQ name intValue =
    printL [name, " = ", (Int.toString intValue)];

end;        


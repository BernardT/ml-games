(*
 * GetArgs.sml
 *)

signature GETARGS =
sig
  exception BadArgument;
  exception NegativeArgument;
  exception NoArgument;

  val get_arg_pos_int: string list -> int
end;

structure GetArgs: GETARGS =
struct
  exception BadArgument;
  exception NegativeArgument;
  exception NoArgument;

  fun get_arg_pos_int [] = raise NoArgument
    | get_arg_pos_int (h::t) = 
    case (Int.fromString h)
      of NONE => raise BadArgument
       | SOME m => if m > 0
                   then m
                   else raise NegativeArgument;
end;

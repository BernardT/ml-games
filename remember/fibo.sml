(*
 * fibo.sml
 * calcul des nombres de Fibonacci avec memoïzation
 * calcul jusqu'à 46 pour les entiers 32 bits
 *
 * compilation:
 * polyc -o fibo fibo.sml
 *)

use "../blib/BtIO.sml";
use "../blib/ShowResults.sml";
use "../blib/GetArgs.sml";

open ShowResults GetArgs;

fun fibo_maker n =
(* 
 * fibo_maker est une fermeture qui mémorise le tableau des
 * valeurs de la suite
 *
 * utiliser le CPS pour assouplir l'affichage des résultats
 * serait une bonne, très bonne chose
 *)
let

  (* dimension du tableau de la mémoïzation *)
  val array_dim = if n > 2
                  then n + 1
                  else 3

  (* création du tableau pour la mémoïzation *)
  val  fibo_values : IntInf.int option array = Array.array(array_dim, NONE);

  (* la fonction de calcul de la suite, la fonction réellement renvoyée *)
  fun get_fibo 0 = 0
    | get_fibo 1 = 1
    | get_fibo k =
    case Array.sub (fibo_values, k)
      of SOME fibo_value => fibo_value
       | NONE =>
           let
             val fibo_value = get_fibo (k - 1) + get_fibo (k - 2);
           in
             Array.update (fibo_values, k, SOME fibo_value);
             fibo_value
           end
in
  get_fibo
end;


fun main () =
let
  val arg1 = get_arg_pos_int (CommandLine.arguments ());
  val fbm = fibo_maker arg1;
in
  show_all_int_results "fibo " fbm arg1;
  print ""
end
handle GetArgs.NoArgument => print "We need an argument (positive integer)\n"
  | GetArgs.BadArgument => print "The argument must be a positive integer\n"
  | GetArgs.NegativeArgument => print "The argument must be a positive integer\n";

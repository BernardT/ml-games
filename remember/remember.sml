(*
 * remember.ml
 * se souvenir des bases de ML
 *)

use "../blib/BtIO.sml";
use "../blib/ShowResults.sml";
use "../blib/GetArgs.sml";

open ShowResults GetArgs;

(*
 * Cette factorielle illustre bien
 * l'utilisation de fonctions imbriquées permettant
 * la récursion terminale.
 * En plus, on utilise la définition d'une fonction avec les filtres.
 * Et on lance une exception en cas de valeur négative de l'argument.
 *)
signature FACT =
sig
  val fact: int -> IntInf.int
  val fact2: int -> IntInf.int
  val inner_fact: IntInf.int -> int -> IntInf.int
end;

structure Fact: FACT =
struct
  (*
   * cette factorielle illustre bien
   * l'utilisation de fonctions imbriquées permettant
   * la récursion terminale.
   *)
  fun fact n =
  let
    fun inner_fact acc k =
      if k = 0
      then acc
      else inner_fact (acc * k) (k - 1);
  in
    if n < 0
    then raise GetArgs.NegativeArgument
    else inner_fact 1 n
  end;


  fun inner_fact acc 0 = acc
    | inner_fact acc k = inner_fact (acc * k) (k - 1);

  fun fact2 n =
    if n < 0
    then raise GetArgs.NegativeArgument
    else inner_fact (IntInf.toLarge 1) n;
end;

fun main () =
let
  val arg1 = get_arg_pos_int (CommandLine.arguments ());
in
  show_all_int_results "fact " Fact.fact arg1;
  print "\n";
  show_all_int_results "fact2 " Fact.fact2 arg1;
  print "\n"
end
handle GetArgs.NoArgument => print "We need an argument (positive integer)\n"
  | GetArgs.BadArgument => print "The argument must be a positive integer\n"
  | GetArgs.NegativeArgument => print "The argument must be a positive integer\n";

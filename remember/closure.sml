(*
 * closure.sml
 * les fermetures en SML
 * dont une partie sans docs, internet cassé!
 *
 * usage :
 * poly -q --use closure.sml [- loops] 
 * compilation :
 * polyc -o closure closure.sml
 *)

use "utils.sml";
use "../blib/BtIO.sml";

open Utils;

(*
 * constantes, une sorte d'enum
 *)
datatype cpt_actions = cpt_reset | cpt_inc | cpt_dec | cpt_get;

(*
 * make_compteur (start, inc) :
 * renvoie une fermeture gérant un compteur
 * - valeur de départ : start,
 * - valeur d'incrément/décrément : inc
 *
 * La fermeture accepte pour paramètre une des constantes cpt_xxx
 * définies plus haut.
 * - cpt_reset : remet le compteur à la valeur start,
 * - cpt_inc : incrémente le compteur de la valeur inc,
 * - cpt_dec : décrémente le comdans le _wiki_, pteur de la valeur inc.
 *)

fun make_compteur start inc =
let
  (* le compteur  est une référence sur start *)
  val inner_cpt : int ref = ref start;

  (* la fonction retournée *)
  fun compteur action =
  let

    (* exécution de l'action *)
    fun icompteur cpt_reset =
        let
          val _ = inner_cpt := start;
        in
          !inner_cpt
        end

      | icompteur cpt_inc =
        let
          val _ = inner_cpt := !inner_cpt + inc;
        in
          !inner_cpt
        end

      | icompteur cpt_dec =
        let
          val _ = inner_cpt := !inner_cpt - inc;
        in
          !inner_cpt
        end

      | icompteur cpt_get = !inner_cpt;

  in
    icompteur action
  end

in
  compteur
end;


(* création d'un premier compteur *)
val c1 = make_compteur 0 1;
(* création d'un second compteur *)
val c2 = make_compteur 20 1;
(* le test *)
fun test_n n =
let

  fun dotest () = (
    c2 cpt_inc;
    BtIO.printValEQ "-> v1" (c1 cpt_dec);
    BtIO.printValEQ "   v2" (c2 cpt_get);
    print ("\n")
  );

  fun iloop 0 = dotest ()
    | iloop k = (
      dotest();
      iloop (k - 1)
    );

in
  iloop n
end;


fun get_args [] = BtIO.onError "We need an integer as argument!!!"
  | get_args (h::t) =
    let
      fun on_number [] = BtIO.onError "We need an integer as argument!!!"
        | on_number (h::t) = 
          case (Int.fromString h)
            of NONE => BtIO.onError "the argument must be an integer!!!"
             | SOME n => if n > 0
                         then n
                         else BtIO.onError "the argument must be a POSITIVE integer";
    in
      if h = "-"
      then on_number t
      else get_args t
    end;

fun main() =
let
  val loops = get_args(CommandLine.arguments ())
in    
  test_n loops;
  c1 cpt_reset;
  test_n loops;
  print ("-------------------------------------\n");
  (* 
   * nécessaire pour sortir de l'interpréteur
   * PAS du compilateur mais c'est plus propre
   *)
  exit OS.Process.success;
  (* 
   * nécessaire pour éviter un warning désagréable dans l'interpréteur
   *)
  (* OS.Process.success *)
  (* 
   * nécessaire pour éviter une erreur désagréable dans le compilateur
   *)
   {} 
end;  



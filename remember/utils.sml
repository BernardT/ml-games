(*
 * utils.sml
 *)

(*
 * imprime un entier
 *) 

signature UTILS =
sig
  val printVal: int -> unit;
  val exit: OS.Process.status -> OS.Process.status
end;

structure Utils: UTILS =
struct
  fun printVal x = print (Int.toString x);


  fun exit exitCode = OS.Process.exit exitCode;
end;  

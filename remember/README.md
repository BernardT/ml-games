remember
========

Ce répertoire porte bien son nom, les programmes sont... disons des exercices de _révision_, en effet, ce n'est pas la première fois que je fricote avec les langages de la famille _ML_.

closure.sml
-----------

Un premier exemple plutôt artificiel de fermeture, ici un compteur, qui a l'avantage de réunir plusieurs points intéressant de la programmation avec ML.

fibo.sml et fibo.fs
-------------------

Un calcul de la suite de Fibonacci avec _mémoïsation_. Il y a la version _standard ML_ (__fibo.sml__) et la version _F#_ dans le répertoire _fsharp_ avec, à quelques détails près, les mêmes fonctionnalités et la même structure.

De cette manière on voit bien les différences entre ces deux branches de _ML_.

remember.sml
------------

C'est le premier programme que j'ai écrit. Très simple au début, il s'est étoffé de ce dont j'avais besoin. Il calcule les factorielles.

TestSig.sml
-----------

Pour m'entraîner avec les signatures...

les fichiers _.mlb_ et _Makefile_
---------------------------------

Le __Makefile__ permet de compiler tout ce petit monde avec __PolyML__. Les fichiers __.mlb__ sont les fichiers de compilations pour __mlton__ qui n'est plus/pas encore traité.

Les exécutables et les fichiers temporaires se retrouvent dans un sous-répertoire nommé _o_. Les cibles de compilation sont :

* _smlexes_ : compilation, avec _PolyML_, des sources en _*.sml_,
* _clean_ : nettoie les exécutables et les éventuels résidus de compilation.

__Note importante__ : __Makefile__ est fait pour la version _GNU_ de _make_. Cela signifie que sous BSD, il faut utiliser _gmake_.

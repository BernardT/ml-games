# note

Après une inactivité de plus d'un an, je reprend ces expérimentations avec pour seuls environnements _PolyML_ et _F#_. _PolyML_ car il est très portable, facile à compiler sur n'importe quel _Unix_, très souple d'utilisation et le projet est toujours actif. _F#_, moins facile à compiler sur divers _Unix_, il me permet d'avoir un lien, même faible, avec _.NET_, ce qui peut toujours aider pour (re)trouver du boulot les jours de vaches maigres. Il m'arrive parfois d'utiliser le _SML/NJ_ original ainsi que _MLTon_ son compilateur.

# ml-games

On notera que _SML/NJ_ fait partie des ancêtres de _OCaml_, de l'INRIA, lui-même ancêtre de _F#_ crée par Microsoft.

On notera aussi ma tendance à pratiquer la _programmation paresseuse_ qui consiste à ouvrir l'éditeur de texte, surveiller twitter, s'occuper du repas, revenir à l'éditeur et ainsi de suite. Ça prend du temps...

## pourquoi _ML_

C'est un langage fonctionnel sans orientation objet (à la base), deux points qui me conviennent, l'orientation objet de la programmation ne me plaît plus vraiment.

On trouvera bien quelques exemples de code en _F#_, membre de la famille _ML_, légèrement dissident, il intègre une orientation objet et une syntaxe très différente de celle de la plupart des _ML_ existant.

## y a quoi là dedans

Il y a du code, essentiellement, du texte, le tout étant mis à jour avec plus ou moins de régularité.

Le répertoire __remember__ est là pour (re)débuter avec le langage, ce n'est pas la première fois que je l'explore, mais c'est la première fois que je m'y met vraiment pleinement. On y trouve un ensemble de programmes assez courts qui devraient aider le débutant et me remémorer rapidement la syntaxe lorsque je n'y touche pas suffisamment.

Le répertoire __blib__ contient un ensemble de fichiers permettant de faire une _bibliothèque_, bien grand mot pour l'instant, et m'évitant de faire d'incessants copier/coller d'un source à l'autre.

Si les curieux explorent les autres répertoires, ils seront déçus pour le moment. J'espère que cela changera rapidement.

# outils

## Monodevelop

L'utilisation de _F#_ est plus simple avec les _projets_ et les _solutions_ produits par _Visual Studio_ ou _Monodevelop_. Sous _Linux_, et en particulier _Ubuntu_, l'installation de _Monodevelop_ fournie dans les paquets par défaut offre un outil totalement instable. Il est bien plus intéressant de faire l'installation par `FlatPak` comme il est décrit sur le site d'origine. Une fois l'installation effectuée, il suffit de lancer la commande :

```bash
flatpak run com.xamarin.MonoDevelop
```
